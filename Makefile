define render_notebook
	jupyter nbconvert --to notebook --execute --ExecutePreprocessor.timeout=None --allow-errors notebooks/unrendered/$(1) --output-dir='./notebooks/rendered' --output $(1)
endef

.PHONY : all
all:  install demo

.PHONY : demo
demo :
	$(call render_notebook,demo.ipynb)

.PHONY : install
install :
	$(call render_notebook,install.ipynb)

.PHONY : clean
clean :
	/bin/rm -r ./notebooks/rendered/*
